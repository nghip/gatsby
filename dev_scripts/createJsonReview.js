var fs = require('fs');
var path = require('path');
let review = [];
var stream = fs.createWriteStream(__dirname + "/../src/db/review/master.json", {flags:'w'});
let totalFiles = 0;

function readFiles(dirname, onFileContent, onError) {

  fs.readdir(dirname, function(err, filenames) {

    if (err) {
      onError(err);
      return;
    }
    
    totalFiles = filenames.length - 1;
    
    filenames.forEach(function(filename, index) {

      fs.readFile(dirname + filename, 'utf-8', function(err, content) {
        if (err) {
          onError(err);
          return;
        }
        onFileContent(filename, content, index);
      });

    });

  });

}

readFiles('./src/pages/review/', function(filename, content, index) {
	let regExpAttribute = /([a-zA-Z]*)\s*=\s*\"([^"]*)\"/g;
	let contentFiltered = content.replace('\\t','');
	let matches = contentFiltered.match(regExpAttribute);
	let attribute = {};

	if (matches) {

		matches.forEach( (match) => {
			let matchFiltered = match.replace(/\"/g,'');
			let keyPair = matchFiltered.split('='); 
			attribute[keyPair[0].trim()] = keyPair[1].trim();
		})

		review.push(attribute);

		let jsonContent = JSON.stringify(attribute, null, 4);
		let path = __dirname + "/../src/db/review/" + filename.replace('.js','') + ".json";
		const fs = require('fs');

		fs.writeFile(path, jsonContent , function(err) {
		    if(err) {
		        return console.log(err);
		    }
		    console.log("The file was saved!");
		}); 

		if (index === totalFiles)
			stream.write(JSON.stringify({'review': review}, null, 4));
	}

}, function(err) {
	throw err;
});