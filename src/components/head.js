import React, { Component } from "react"
import { Helmet } from "react-helmet"

class Head extends Component {

    render() {

    	const { title, meta, canonical } = this.props;

        return (
			<Helmet>
				<meta charSet="utf-8" />
				<title>{ title }</title>
				{/* window.location is different on production <link rel="canonical" href={ canonical ? window.location.origin + canonical : "" } /> */}
				<link rel="canonical" href={ canonical ? canonical : "" } />
				<meta name="description" content={ meta } />
			</Helmet>
        );
        
    }

}

export default Head;
