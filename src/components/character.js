import React, { Component } from "react"

class Character extends Component {

    render() {

        const { character } = this.props;

        return (
            <div>
				{ character.name }
				{ character.type }
				{ character.description }
            </div>
        );
        
    }

}

export default Character;
