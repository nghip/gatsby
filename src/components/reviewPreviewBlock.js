import React, { Component } from "react"

class ReviewPreviewBlock extends Component {

    render() {
    	const { review } = this.props;

        return (
			<div>
				<a href={review.canonical}>{ review.title }</a>
			</div>
        );
        
    }

}

export default ReviewPreviewBlock;
