import React, { Component } from "react"
import "../styles/home.scss"

class Home extends Component {

    render() {

        return (
            <div role="main" className="home">
				{this.props.children}
            </div>
        );
        
    }

}

export default Home;
