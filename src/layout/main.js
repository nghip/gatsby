import React, { Component } from "react"
import Head from "../components/head"
import Header from "../components/header"
import Footer from "../components/footer"
import Home from "./home"
import "../styles/main.scss"

class Main extends Component {

    layout = {
    	// 'page': Page,
        'home': Home,
    }

    getLayout = () => {
    	const { selection } = this.props;
        const ComponentName = this.layout.hasOwnProperty(selection) ? this.layout[selection] : false;

        if (ComponentName)
	        return (
				<ComponentName>
					{this.props.children}
				</ComponentName>
	        )

    	return this.props.children;
    }

    render() {

        const { title, meta, canonical } = this.props;

        return (
            <div>
		  		<Head 
		  			title={ title }
		  			meta={ meta }
		  			canonical={ canonical }
		  		/>

				<Header />

				{this.getLayout()}

                <Footer />

            </div>
        );
        
    }

}

export default Main;
