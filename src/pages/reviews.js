import React from "react"
import Layout from "../layout/main"
import master from "../db/review/master.json"
import ReviewPreviewBlock from "../components/reviewPreviewBlock"

//Future - If the master files becomes to large then we can split it into parts for pagination.

const reviews = () => {

	let reviews = [];
	for (const file of master.review) {
		reviews.push(
			<ReviewPreviewBlock key={file.canonical} review={file} />
		)
	}
	return reviews;
}

export default () => (
  	<Layout
		title="Reviews"
		meta="List of Reviews"
		canonical="/reviews"
		selection="Page"
  	>

  		{reviews()}

  	</Layout>
)