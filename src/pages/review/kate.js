import React from "react"
import Layout from "../../layout/main"
import Kate from "../../db/character/joe.json"
import Character from "../../components/character"

export default () => (
  	<Layout
		title = "Kate Review"
		meta = "This is a meta"
		canonical = "/review/kate"
		selection = "page"
		category = "character"
		rating = "2"
		summary = "Kate the best x 2"
		publishedDate = "06/15/10"
  	>
		<Character character={ Kate } />
		<p>Such wows Vry { Kate['name'] }</p>
  	</Layout>
)