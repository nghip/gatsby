import React from "react"
import Layout from "../../layout/main"
import Joe from "../../db/character/joe.json"
import Character from "../../components/character"

export default () => (
  	<Layout
		title = "Joe Review"
		meta = "This is a meta"
		canonical = "/review/joe"
		selection = "page"
		category = "character"
		rating = "2"
		summary = "Joe the best x 2"
		publishedDate = "06/15/19"
  	>
		<Character character={ Joe } />
		<p>Such wows Vry { Joe['name'] }</p>
  	</Layout>
)