import React from "react"
import Layout from "../layout/main"

export default () => (
  	<Layout
		title="test love"
		meta="Home Page"
		canonical="/"
		selection="home"
  	>
  		-- MAIN CONTENT STARTS HERE --
  		<p>HOME PAGE</p>
		-- MAIN CONTENT ENDS HERE --
  	</Layout>
)