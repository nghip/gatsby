import React from "react"
import Layout from "../layout/main"

export default () => (
  	<Layout
		title="Page Not Found - 404"
		meta="Oops Page Not Found"
		canonical={false}
		selection="page"
  	>
  		-- MAIN CONTENT STARTS HERE --
  		404
		-- MAIN CONTENT ENDS HERE --
  	</Layout>
)