import React, { Component } from "react"
import Socket from "../websocket/index"

class RockPaperSissor extends Component {

	constructor(props) {
		super(props);

		this.state = {
			timestamp: 'no timestamp yet',
			"message" : "cat",
			"winner" : "0",
			"room" : ""
		};
	}

	componentDidMount() {
		Socket.on( 'notification', this.setNotification );
	}

	componentWillUnmount() {
		Socket.off( 'notification');
	}

	setNotification = (data) => {
		this.setState({ "message": data.message });
	}

	createGame = (e) => {
		Socket.emit('createGame');
	}

	joinGame = (e) => {
		Socket.emit('joinGame', { room: this.state.room });
	}

	updateRoom = (e) => {
		this.setState({ "room": e.target.value });
	}

	cpuRandom = () => {
		let rng = Math.floor(Math.random() * Math.floor(3));
		let rps = ['rock','paper','sissor'];

		return rps[rng];
	}

	getWinner = (playerOneChoice, playerTwoChoice) => {

		let condition = {
			rock : ['sissor'],
			paper : ['rock'],
			sissor : ['paper']
		}

		let playerOneWins = condition[playerOneChoice].find( ( type ) => {
			return type === playerTwoChoice
		});

		let playerTwoWins = condition[playerTwoChoice].find( ( type ) => {
			return type === playerOneChoice
		});

		if (playerOneWins)
			return 1;

		if (playerTwoWins)
			return 2;

		return -1; // draw

	}

	selection = (e) => {

		let handShape = e.target.className;
		Socket.emit('turn', { handShape: handShape });


		// let playerOneChoice = e.target.className;
		// let playerTwoChoice = this.cpuRandom();

		// let winner = this.getWinner(playerOneChoice,playerTwoChoice);

		// if (winner !== -1)
		// 	this.setState({ "message": `Player ${winner} Wins`, "winner" : winner});
		// else
		// 	this.setState({ "message": `Draw`, "winner" : winner});
	}

    render() {
        return (
            <div>
            	<button onClick={ (e) => this.createGame(e) } className="createGame">create Game</button>

            	<div className="">
	            	<input id="joinGameInput" name="room" value={ this.state.room } onChange={ this.updateRoom } />
	            	<button id="joinGameSubmit" onClick={ (e) => this.joinGame(e) }>Join</button>
	            </div>

            	<div className="notification">{this.state.message}</div>

				<div onClick={(e) => this.selection(e)} type="rock" className="rock">R</div>
				<div onClick={this.selection} type="paper" className="paper">P</div>
				<div onClick={this.selection} type="sissor" className="sissor">S</div>
            </div>
        );
        
    }

}

export default RockPaperSissor;
