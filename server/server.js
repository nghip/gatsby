const io = require('socket.io')();

let rooms = [];
let states = {};

makeid = (length) => {

	let result           = '';
	let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	let charactersLength = characters.length;
	let exist = false;
	let attempt = 0;
	let maxAttempt = 5;

	do {
		//reset
		exist = false;
		result = ''; 

		for ( var i = 0; i < length; i++ ) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}

		exist = rooms.find( (e) => {
			return e === result
		});

		++attempt;

	} while( exist && attempt < maxAttempt ); //exit if not duplicate & Prevent infinite loop

	rooms.push(result);

	return 'cat';

}

test = () => {
	console.log('asd');
}

initSettings = (room, clients) => {

	// let state = {};
	// state.room = room;

	// console.log('ss');

	// console.log(io.sockets.clients('room'));

}

io.on('connection', (socket) => {

    socket.on('createGame', (data) => {

    	let id = makeid(5);

    	if (id) {
	        socket.join(id);
	        console.log(socket.id);
	        socket.emit('notification', { message: id });
	    } else {
	    	socket.emit('error', { message: "Error can't create room" });	
	    }

    });

    socket.on('joinGame', function (data) {

    	console.log('player attempting to join room');

        let room = io.sockets.adapter.rooms[data.room];

        if (room && room.length === 1) {
            socket.join(data.room);
            socket.broadcast.to(data.room).emit('notification', { message: "An opponent joined the room" });

            // test();
			// socket.emit('player2', { name: data.name, room: data.room })
// data.room, io.sockets.clients('room')
		console.log(socket.id);

            initSettings();

        } else {
            socket.emit('err', { message: 'Sorry, The room is full!' });
        }

    });

});

const port = 4000;
io.listen(port);
console.log('listening on port ', port);
